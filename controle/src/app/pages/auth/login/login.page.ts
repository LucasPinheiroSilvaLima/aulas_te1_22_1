import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { LoginService } from '../service/login.service';
import { User } from "firebase/auth"
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.loginForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  isUserLoggedIn() {
/*
    this.service.isLoggedIn._subscribe(user => {
      if(user){
        console.log(user);

        this.nav.navigateForward('home');
      }
    }
    
    );
  */  
  }

  login() {

    const user = this.loginForm.value;
    this.service.login(user);

  }

  

}
