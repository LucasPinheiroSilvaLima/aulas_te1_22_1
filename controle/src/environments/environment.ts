// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:  {
    apiKey: 'AIzaSyCKoC008vmbUr-BcHCfUb3Df4pUff5PcEE',
    authDomain: 'controle-if-lucaslima.firebaseapp.com',
    projectId: 'controle-if-lucaslima',
    storageBucket: 'controle-if-lucaslima.appspot.com',
    messagingSenderId: '340152330655',
    appId: '1:340152330655:web:7d4f10cffc4786a0b7f5aa',
    measurementId: 'G-P7HZNCLMY0'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
